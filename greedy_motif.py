#!/usr/bin/python

import sys
import re

final_list = ['a','b','c']
max_score = 500
linescounted = 1

def get_input(path):
    with open (path) as f:
        numbers = f.next().split()
        sequence = []
        for line in f:
            sequence.append(line.strip())
    k = int(numbers[0])
    t = numbers[1]
    return sequence,k,t

def get_kmers(sequence,k,t):
    seq = sequence[0]
    for i in range(len(seq) - k  + 1):
        kmer = seq[i:i+k]
        create_profile(sequence,seq,kmer,t,k) 

def create_profile(sequence,seq,kmer,t,k):
    global final_list
    global max_score
    global linescounted
    linescounted = 1
    temp_list = []
    profile = dict()
    profile['A'] = [0] * k
    profile['C'] = [0] * k
    profile['G'] = [0] * k
    profile['T'] = [0] * k
    temp_list.append(kmer)
    it = 0
    for j in kmer:
        profile[j][it] += 1 
        it += 1

    for s in sequence[1:]:
        profile,new_kmer = find_best_kmer(profile,s)
        temp_list.append(new_kmer)
        print "Profile:"
        for p in profile:
            sys.stdout.write(p + ' : ')
            for i in profile[p]:
                sys.stdout.write(str(i) + '\t')
            sys.stdout.write('\n')
    linescounted += 1   
    consensus = get_consensus(profile)
    test_consensus(consensus,temp_list)
#   print "temp_list:"
#   print  temp_list
#    print final_list
#    print consensus

def find_best_kmer(profile,s):
#   prob_matrix = create_prob_matrix(profile)
    max_s = 0
    max_k = ''
    for i in range(len(s) - k + 1):
        curr_kmer = s[i:i+k]
        curr_s = 0.0
#        print "Test Block ==========================="
#        print curr_kmer
        for j in range(len(curr_kmer)):
            curr_s += profile[curr_kmer[j]][j]  
#            print "j " + str(j)
#            print "curr_s " + str(curr_s)
#            print "nuc " + curr_kmer[j]
#            print "prof nuc " + str(profile[curr_kmer[j]][j])  
#        print "End ============================="
        if curr_s > max_s:
            max_s = curr_s      
            max_k = curr_kmer
    
    for j in range(len(max_k)):
        profile[max_k[j]][j] += 1 
    return profile,max_k

def get_consensus(profile):
    consensus = []
    for i in range(k):
        ret = 'A'
        retnum =  profile['A'][i]
        if  profile['C'][i] > retnum:
            retnum = profile['C'][i]
            ret = 'C'
        if  profile['G'][i] > retnum:
            retnum = profile['G'][i]
            ret = 'G'
        if  profile['T'][i] > retnum:
            retnum = profile['T'][i]
            ret = 'T'
        consensus.append(ret)    
    return ''.join(consensus)       

def test_consensus(consensus,temp_list):
    global max_score
    global final_list
    temp_score = 0
#   print consensus
#   print temp_list
    for seq in temp_list:
#       print seq
        for i in range(len(consensus)):
            if seq[i] != consensus[i]:
                temp_score += 1
#    print "======= Test Block ========"
#    print temp_list
#    print consensus
#    print temp_score
#    print max_score
#    print "======= END =============="
    if temp_score <= max_score:
        max_score = temp_score
        final_list = temp_list

def create_prob_matrix(profile):
    prob_matrix = dict()
    prob_matrix['A'] = [0] * k
    prob_matrix['C'] = [0] * k
    prob_matrix['G'] = [0] * k
    prob_matrix['T'] = [0] * k
    for i in profile:
        for j in profile[i]:
            prob_matrix[i][j] = float(profile[i][j]) / float(linescounted) 
    return prob_matrix 

sequence,k,t = get_input(sys.argv[1])
get_kmers(sequence,k,t)
#print "final one"
#print " ".join(final_list)
for i in final_list:
    print i
