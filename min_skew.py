#!/usr/bin/python

import sys
import re

with open (sys.argv[1]) as f:
    seq = f.next()

skew = 0
skewList =[]
minList=[]

for i in seq:
    if i == "G":
        skew += 1
    elif i == "C":
        skew -= 1
   #sys.stdout.write(str(skew))
    skewList.append(skew)

minVal = min(skewList) 

count = 0
for i in skewList:
    count += 1
    if i == minVal:
        minList.append(count)

print minList

