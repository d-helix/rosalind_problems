#!/usr/bin/python

import re
import sys

def get_input(path):
    with open (path) as fh:
        seq = fh.next().strip()
    return seq

def build_array(seq):
    suffixes = []
    for i in range(len(seq)):
        suffixes.append(seq[i:] + str(i))
        suffixes.sort()
    return suffixes

seq = get_input(sys.argv[1])
arr = build_array(seq)

for entry in arr:
    sys.stdout.write(entry[entry.index('$') + 1:] + ' ')
print
