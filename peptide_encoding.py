#/usr/bin/python

import sys
import re
import string

def GetInput(path):
    with open (path) as f:
        sequence = f.next().strip()
        peptide = f.next().strip()
    return sequence, peptide

def FindPeptideSeqs(sequence,peptide,codons):
    foundSeq = []
    winlength = len(peptide) * 3
    for i in xrange(len(sequence) - winlength):
        forwardSeq = sequence[i:i + winlength] 
        reverseSeq = forwardSeq[::-1].translate(string.maketrans("ATGC","TACG")) 
        fCodons = re.findall('.{1,3}',forwardSeq)
        rCodons = re.findall('.{1,3}',reverseSeq) 
        fPeptide, rPeptide = "", ""
        for codon in fCodons:
            fPeptide += codons[codon]
        for codon in rCodons:
            rPeptide += codons[codon]
        if fPeptide == peptide or rPeptide == peptide:
            foundSeq.append(forwardSeq)
    return foundSeq  


codons = {"ATT":"I","ATC":"I","ATA":"I",
          "CTT":"L","CTC":"L","CTA":"L","CTG":"L","TTA":"L","TTG":"L",
          "GTT":"V","GTC":"V","GTA":"V","GTG":"V",
          "TTT":"F","TTC":"F",
          "ATG":"M",
          "TGT":"C","TGC":"C",
          "GCT":"A","GCC":"A","GCA":"A","GCG":"A",
          "GGT":"G","GGC":"G","GGA":"G","GGG":"G",
          "CCT":"P","CCC":"P","CCA":"P","CCG":"P",
          "ACT":"T","ACC":"T","ACA":"T","ACG":"T",
          "TCT":"S","TCC":"S","TCA":"S","TCG":"S","AGT":"S","AGC":"S",
          "TAT":"Y","TAC":"Y",
          "TGG":"W",
          "CAA":"Q","CAG":"Q",
          "AAT":"N","AAC":"N",
          "CAT":"H","CAC":"H",
          "GAA":"E","GAG":"E",
          "GAT":"D","GAC":"D",
          "AAA":"K","AAG":"K",
          "CGT":"R","CGC":"R","CGA":"R","CGG":"R","AGA":"R","AGG":"R",
          "TAA":"X","TAG":"X","TGA":"X"}

sequence, peptide = GetInput(sys.argv[1])          
found = FindPeptideSeqs(sequence,peptide,codons)
print " ".join(found)




