#! usr/bin/python

import sys

def ParseInput( path ) :
    with open (path) as fh:
        seq = fh.next().strip()
        k = int(fh.next().strip())
    return seq, k

def CountWords(seq, k):
    counts = dict()
    kmers = []

    highestCount = -1
    for i in range(len(seq) - k  + 1):
        kmer = seq[i: i+k]
   #     print kmer

        if kmer in counts:
            counts[kmer] +=1
        else:
            counts[kmer] = 1

        if counts[kmer] > highestCount:
            kmers = []
            kmers.append(kmer)
            highestCount = counts[kmer]
        elif counts [kmer] == highestCount:
            kmers.append(kmer)
   # print highestCount
    for i in kmers:
        sys.stdout.write(i+ " ")
       # print ""
   # print kmers

#print "Assignment 1A: Word counting"

inputFilePath = sys.argv[1]
seq, k = ParseInput(inputFilePath)
CountWords(seq,k)



