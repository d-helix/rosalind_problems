#!/usr/bin/python

from blist import blist

import re
import sys

def get_input(path):
    with open (path) as fh:
        seq = fh.next().strip()
    return seq

def build_array(seq):
    seq = blist(seq)
    suffixes = range(len(seq))
    suffixes.sort(key=lambda i: seq[i:])
    return suffixes

seq = get_input(sys.argv[1])
arr = build_array(seq)

print ', '.join(str(x) for x in arr)
