#!/usr/bin/python

import re
import sys


class Node:

    def __init__(self):
        self.right = 0
        self.down  = 0
        self.value = 0

    def set_down(self,down):
        self.down = down

    def set_right(self,right):
        self.right = right

    def set_value(self,value):
        self.value = value

def get_input( path ) :
    with open (path) as fh:
        nm = fh.next().strip().split(' ')
        n = int(nm[0])
        m = int(nm[1])
        matrix = [[None]*(n+1) for i in range(m+1)]
        print matrix
        for j in range(n+1):
            for i in range(m+1):
                print str(j) + ' ' + str(i)
                matrix[j][i] = Node()
            
        print matrix

        line = fh.next().strip().split(' ')
        row = 0
        while line[0] != '-':
            for i in range(len(line)):
              # matrix[row][i] = Node() 
                matrix[row][i].set_down(line[i])

            line = fh.next().strip().split(' ')
            row += 1
                
        row = 0
        for line in fh:
            print line
            line = line.strip().split(' ')
            for i in range(len(line)):
                matrix[row][i].set_right(line[i])
            row += 1
        
        for m in matrix:
            for i in m:
                sys.stdout.write(str(i.down) + ' ')
            print
        print
        for m in matrix:
            for i in m:
                sys.stdout.write(str(i.right) + ' ')
            print
        print
            
    for i in range(len(matrix[0])):
        if i == 0:
            continue
        matrix[0][i].set_value(int(matrix[0][i-1].value) + int(matrix[0][i-1].right))

    for i in range(n + 1):
        if i == 0:
            continue
        matrix[i][0].set_value(int(matrix[i-1][0].value) + int(matrix[i-1][0].down)) 

    for i in matrix:
        for j in i:
            print 'val: ' + str(j.value) + ' right: ' + str(j.right) + ' down: ' + str(j.down)
        print

    print m
    for i in range(1,n+1):
        for j in range(1,n+1):
            matrix[i][j].value = max(int(matrix[i-1][j].value) + int(matrix[i-1][j].down),(int(matrix[i][j-1].value) + int(matrix[i][j-1].right)))
            print str(i) + ' ' + str(j)
            print str(int(matrix[i-1][j].value) + int(matrix[i-1][j].down)) + ' ' + str((int(matrix[j][i-1].value) + int(matrix[j][i-1].right)))


    for m in matrix:
        for i in m:
            sys.stdout.write(str(i.value) + ' ')
        print

    
    for i in matrix:
        for j in i:
            print 'val: ' + str(j.value) + ' right: ' + str(j.right) + ' down: ' + str(j.down)
        print

get_input(sys.argv[1])
