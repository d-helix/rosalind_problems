#!/usr/bin/python

import re
import sys

def get_input(path):
    with open (path) as fh:
        v = fh.next().strip()
        v = '-' + v
        w = fh.next().strip()
        w = '-' + w
    return v, w

def import_score_matrix(path):
    with open (path) as fh:
        matrix = []
        for line in fh:
            line = line.strip().split()
            matrix.append(line)
#   for m in matrix:
#       print ' '.join(m)
    return matrix

def lcs(v,w,pam):
    graph = [[None] * (len(w)) for i in range(len(v))]
    backtrack = [[None] * (len(w)) for i in range(len(v))]
    
    pam_dict = {}
    for i in range(0,len(pam[0])):
        pam_dict[pam[0][i]] = i

    graph[0][0] = 0
    for i in range(1,len(v)):
        graph[i][0] = 0
    for j in range(1,len(w)):
        graph[0][j] = 0
#   for g in graph:
#       print g
    max_numb = 0
    max_row = 0
    max_col = 0
    for i in range(1,len(v)):
        for j in range(1,len(w)):
            max_list = []
            max_list.append(0)
            max_list.append(graph[i-1][j] - 5)
            max_list.append(graph[i][j-1] - 5)
            max_list.append(graph[i-1][j-1] + 
                    int(pam[pam_dict[v[i]]][pam_dict[w[j]]]))
            graph[i][j] = max(max_list)
            if max(max_list) > max_numb:
                max_numb = max(max_list)
                max_row = i
                max_col = j

            if graph[i][j] == graph[i][j-1] - 5:
                backtrack[i][j] = 'right'
            elif graph[i][j] == graph[i-1][j] - 5: 
                backtrack[i][j] = 'down'

            elif graph[i][j] == (graph[i-1][j-1] + int(pam[pam_dict[v[i]]][pam_dict[w[j]]])):
                    backtrack[i][j] = 'diag'


    for g in graph:
        print g
    print
    for b in backtrack:
        print b

    return backtrack,graph,max_row,max_col

def find_start(bt):
    max_num = 0
    row = None
    col = None
    for i in range(len(bt)):
        cur_max = max(bt[i])
        if cur_max > max_num:
            max_num = cur_max
            row = i
#    print
    for j in range(len(bt[row])):
        if bt[row][j] == max_num:
            col = j
            break
#    print row,col            
    return row,col
            

v,w = get_input(sys.argv[2])
pam_matrix =  import_score_matrix(sys.argv[1])
backtrack,num_graph,row,col = lcs(v,w,pam_matrix)
gr = [['-'] * (len(w) + 1) for i in range(len(v)+1)]
for i in range(1,len(gr)):
    gr[i][0] = v[i-1]
for j in range(1,len(w)+1):
    gr[0][j] = w[j-1]


i = row
j = col
#print
#print i,j
final_one = ''
final_two = ''


go = True
count = 0
while go:
    count += 1
    if i == 0 and j == 0:
        go = False
        gr[i+1][j+1] = 1

    elif j == 0:
        final_one += v[i]
        final_two += '-'
        gr[i+1][j+1] = 1
        i = i-1

    elif i == 0:
        final_one += '-'
        final_two += w[j]
        gr[i+1][j+1] = 1
        j = j - 1

    elif backtrack[i][j] == 'down':
        final_one += v[i]
        final_two += '-'
        gr[i+1][j+1] = 1
        i = i-1
    elif backtrack[i][j] == 'right':
        final_one += '-'
        final_two += w[j]
        gr[i+1][j+1] = 1
        j = j - 1    
    elif backtrack[i][j] == 'diag':
        final_one += (v[i])
        final_two += (w[j])
        gr[i+1][j+1] = 1
        i = i - 1
        j = j - 1 
    else:
        gr[i+1][j+1] = 0
        go = False

print
for i in gr:
    print ' '.join(str(x) for x in i)
print
print num_graph[row][col]
print final_one[::-1]
print final_two[::-1]
