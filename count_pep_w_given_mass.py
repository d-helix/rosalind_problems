#!/usr/bin/python

import sys
import re

def ParseInput(path):
    with open(path) as f:
        number = int(f.next().strip())
    return number

def justDoIt(number):
    weights = [57,71,87,97,99,101,103,113,114,115,128,129,131,137,147,156,163,186]
    count = [0] * (number + 1)
    count[0] = 1
    for i in range(1,number + 1):
        summed = 0
        for weight in weights:
            if i >= weight:
                summed += count[i - weight]
            count[i] = summed 
    print count[number] 

number = ParseInput(sys.argv[1])

justDoIt(number)
