#!/usr/bin/python

import re
import sys

def get_input( path ) :
    with open (path) as fh:
        k = int(fh.next()) 
        seq = fh.next()
    return k,seq


k,seq = get_input(sys.argv[1])
 
kmer_list = []
for i in range(len(seq) - k + 1):
    kmer_list.append(seq[i:(k-1)+i])
    
graph = dict()
for kmer1 in kmer_list:
    for kmer2 in kmer_list:
        if kmer1[1:] == kmer2[0:(k-2)]:
            if kmer1 in graph:
                graph[kmer1].add(kmer2) 
            else:
                graph[kmer1] = set() 
                graph[kmer1].add(kmer2)

for g in graph:
    sys.stdout.write( g + ' -> ')
    sys.stdout.write(','.join(graph[g]))
    print
