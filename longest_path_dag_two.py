#!/usr/bin/python

import re
import sys


def get_input( path ) :
    with open (path) as fh:
        start = int(fh.next().strip())
        end = int(fh.next().strip())
        from_nodes = []
        to_nodes = []
        weights = []
        for line in fh:
            reSearch = re.search(r'(\d+)->(\d+):(\d+)',line)
            from_nodes.append(int(reSearch.group(1)))
            to_nodes.append(int(reSearch.group(2)))
            weights.append(int(reSearch.group(3)))
    return start,end,from_nodes,to_nodes,weights 

def build_graph(start,end,from_nodes,to_nodes,weights):
    size = max(from_nodes + to_nodes)    
    graph = [[-float('inf')] * (size+1) for i in range(size+1)]
    for i in range(len(from_nodes)):
        graph[from_nodes[i]][to_nodes[i]] = weights[i]
    for i in range(len(graph)):
        graph[0][0] = 0
    return graph,size

def run_it(graph,size,start,end,from_nodes,to_nodes,weights):
    distances = [-float('inf')] * (size + 1)
    distances[start] = 0
    bt = [-1] * (size+1)

    for g in graph:
        print g
    
    for i in range(len(graph)):
        for j in range(len(graph)):
            if graph[i][j] > -1 :
                dis = distances[i] + graph[i][j]
                if dis > distances[j]:
                    distances[j] = dis   
                    bt[j] = i
    print distances
    print bt
    final = []
    backtrack(end,bt,final,start)
    print distances[end]
    print '->'.join(str(x) for x in reversed(final))

def backtrack(current,bt,final,start):
    if current == start:
        final.append(current)
        return
    final.append(current)
    backtrack(bt[current],bt,final,start) 


start,end,from_nodes,to_nodes,weights = get_input(sys.argv[1])
graph,size = build_graph(start,end,from_nodes,to_nodes,weights)
run_it(graph,size,start,end,from_nodes,to_nodes,weights)
