#!/usr/bin/python

import re
import sys

def get_input( path ) :
    with open (path) as fh:
        seq_list = []
        for line in fh:
            seq_list.append(line.strip())
    return seq_list

seq_list = get_input(sys.argv[1])

graph = dict()

'''
for seq in seq_list:
    kmer = seq[:-1]
    
    for seq2 in seq_list:
        kmer2 = seq[1:]
        print kmer + ' ' + kmer2
        if kmer[1:] == kmer2[:-1]:
            if kmer not in graph:
                graph[kmer] = set()
            graph[kmer].add(kmer2)
'''

for seq in seq_list:
    kmer = seq[:-1]
    kmer2 = seq[1:]
    if kmer not in graph:
        graph[kmer] = []
    graph[kmer].append(kmer2)

for g in sorted(graph):
    sys.stdout.write(g + ' -> ')
    sys.stdout.write(','.join(graph[g]))
    print


