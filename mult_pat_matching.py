#!/usr/bin/python

import re
import sys

def get_input(path):
    with open (path) as fh:
        seq = fh.next().strip()
    return seq

def run_it(seq):
    print seq
    seqs = []
    bwt = []
    for i in range(len(seq)):
        ins = seq[i:] + seq[:i]
        seqs.append(ins)
    seqs = sorted(seqs)
    for i in seqs:
        bwt.append(i[-1])
    number = {}
    ends = []
    for nuc in bwt:
        if nuc == '$':
            ends.append(nuc)
            continue
        elif nuc not in number:
            number[nuc] = 1
        else:
            number[nuc] += 1
        ends.append(nuc + str(number[nuc]))
    fronts = ['$']
    for nuc in sorted(number):
        for i in range(1,number[nuc]+1):
            fronts.append(nuc + str(i))
    print fronts
    print ends
#for i in range(len(fronts)):
#        print fronts[i] + '---' + ends[i]
   
    original = '$'
    current = '$'
    for i in range(len(ends) - 1):
        current = ends[fronts.index(current)]
        original = current[0] + original
    print original
    return ends
    

seq = get_input(sys.argv[1]) + '$'
ends = run_it(seq)
print ''.join(ends)
