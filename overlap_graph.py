#!/usr/bin/python

import re
import sys

def get_input( path ) :
    with open (path) as fh:
        seq_list = []
        for line in fh:
            seq_list.append(line.strip())
    return seq_list


seq_list = get_input(sys.argv[1])
 
final_list = []
for seq in seq_list:
    front = seq[0:len(seq)-1]
    end = seq[1:len(seq)]
    for f in seq_list:
        if front == f[1:len(seq)]:
            final_list.append(f + " -> " + seq)
        if end == seq[0:len(seq)-1]:
            final_list.append(seq + " -> " + f)

final_list = sorted(final_list)

for i in final_list:
    print i
