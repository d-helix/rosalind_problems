#!/usr/bin/python

import sys
import re

matrix = dict()
with open (sys.argv[1]) as f:
    seq = f.next().strip()
    k = int(f.next().strip())
    matrix['A'] = [float(i) for i in f.next().strip().split()]
    matrix['C'] = [float(i) for i in f.next().strip().split()]
    matrix['G'] = [float(i) for i in f.next().strip().split()]
    matrix['T'] = [float(i) for i in f.next().strip().split()]


max_kmer = ''
max_kmer_score = 0
for i in range(len(seq) - k  + 1):
    kmer = seq[i: i+k]
    score = 0
    loc = 0
    for nuc in kmer:
        score += matrix[nuc][loc]
        loc += 1
    if score > max_kmer_score:
        max_kmer_score = score 
        max_kmer = kmer

print max_kmer 
   

