#!/usr/bin/python

import sys
import re

with open (sys.argv[1]) as f:
    pattern = f.next()
    sequence = f.next()
    mismatches = int(f.next())

locations = []

for i in range(len(sequence) - len(pattern) + 1):
    kmer = sequence[i:i + len(pattern) - 1]
    misCount = 0
    for j in range(len(pattern) - 1):
       if pattern[j] != kmer[j]:
           misCount += 1
    if misCount <= mismatches:
        locations.append(str(i))

print (" ".join(locations))    
