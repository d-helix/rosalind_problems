#!/usr/bin/python

import re
import sys

with open (sys.argv[1]) as fh:
    a = fh.next().strip()
    b = fh.next().strip()

max_length = 0
max_str = ''
test_int = 1

while test_int <= len(a):
#print a
    test_str = a[:test_int]
    while test_str in b:
        max_length = len(test_str)
        max_str = test_str
        test_int += 1     
        test_str = a[:test_int]
    a = a[1:]

print max_str
