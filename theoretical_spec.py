#!/usr/bin/python

import sys
import re

def GetInput(path):
    with open (path) as f:
        peptide = f.next().strip()
    return peptide

def justDoIt(incoming,AminoMasses):
    masses = []
    masses.append('0')
    testme = True
    for i in xrange(len(incoming)):
        if testme:
            chunk = incoming[i:] + incoming[:i]
            testme = False
        else:
            chunk = incoming[i:] + incoming[:(i-1)]
        while chunk:
            total = 0
            for aa in chunk:
                total += AminoMasses[aa]
            masses.append(str(total)) 
            chunk = chunk[:-1] 
    return sorted(masses)

AminoMasses = {
    'G': 57,
    'A': 71,
    'S': 87,
    'P': 97,
    'V': 99,
    'T': 101,
    'C': 103,
    'I': 113,
    'L': 113,
    'N': 114,
    'D': 115,
    'K': 128,
    'Q': 128,
    'E': 129,
    'M': 131,
    'H': 137,
    'F': 147,
    'R': 156,
    'Y': 163,
    'W': 186  }


peptide = GetInput(sys.argv[1])
numbers = justDoIt(peptide,AminoMasses)

print " ".join(numbers)




