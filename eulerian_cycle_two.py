#!/usr/bin/python 

import re
import sys

#===========<[ Functions ]>============#
def get_input( path ) :
    with open (path) as fh:
        graph = dict()
        for line in fh:
            line = line.strip().split()
            graph[line[0]] = line[2].split(',')
    return graph

def get_path(i,graph):
    result = []
    result.append(i)
    while len(graph[i]) != 0:
        i = graph[i].pop(0)
        result.append(i)
    return result

def get_num(graph):
    num = 0
    for i in graph:
        num += len(graph[i])
    return num 

def merge(final,current,i):
    result = []
    for j in range(len(final)):
        if j == i:
            for k in current:
                result.append(k)
        else:
            result.append(final[j]) 
    return result
        
def find_cycle(graph):
    final = get_path(graph.keys()[0],graph)
    while get_num(graph) != 0:
        for i in range(len(final)):
            if len(graph[final[i]]) != 0:
                current = get_path(final[i],graph)
                final = merge(final,current,i)
    return final


#-------------- Main ----------------#
graph = get_input(sys.argv[1])
cycle = find_cycle(graph)
print "->".join(cycle)
