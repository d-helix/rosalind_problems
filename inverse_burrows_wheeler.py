#!/usr/bin/python

import re
import sys

def get_input(path):
    with open (path) as fh:
        seq = fh.next().strip()
    return seq

def get_orig(bwt):
    number = {}
    ends = []
    for nuc in bwt:
        if nuc == '$':
            ends.append(nuc)
            continue
        elif nuc not in number:
            number[nuc] = 1
        else:
            number[nuc] += 1
        ends.append(nuc + str(number[nuc]))
    fronts = ['$']
    for nuc in sorted(number):
        for i in range(1,number[nuc]+1):
            fronts.append(nuc + str(i))

    original = '$'
    current = '$'
    for i in range(len(ends) - 1):
        current = ends[fronts.index(current)]
        original = current[0] + original
    return original
        
bwt = get_input(sys.argv[1])
print get_orig(bwt)

