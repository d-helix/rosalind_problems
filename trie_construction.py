#!/usr/bin/python

import re
import sys


class Node:
    def __init__(self,val,in_id):
        self.node_list = []
        self.value = val
        self.node_id = in_id

    def node_vals(self):
        node_vals = []
        for n in self.node_list:
            node_vals.append(n.value)
        return node_vals

    def get_node(self,gnode):
        for n in self.node_list:
            if n.value == gnode:
                return n


def get_input( path ) :
    seqs = []
    with open (path) as fh:
        for line in fh:
            seqs.append(line.strip())
    return seqs

def print_trie(current):
    for n in current.node_list:
        print str(current.node_id) + ' ' +str(n.node_id) + ' ' + n.value
        print_trie(n)

def build_trie(seqs):
    root = Node('root',1)
    current_node = root
    ident = 2
    for seq in seqs:
        for nuc in seq:
            if nuc in current_node.node_vals():
                current_node = current_node.get_node(nuc)
            else:
                current_node.node_list.append(Node(nuc,ident))
                current_node = current_node.get_node(nuc)
                ident += 1
        current_node = root
    return root

if __name__ == "__main__":
    seqs = get_input(sys.argv[1])
    trie = build_trie(seqs)
    print_trie(trie)
