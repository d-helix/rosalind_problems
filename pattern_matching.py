#! usr/bin/python

import sys
import re

def parseInput(path):
    with open (path) as fh:
        sub = fh.next().strip()
        theString = fh.next().strip()
    return sub, theString

def patternMatch(sub, seq):
    locations = []
    for i in range(len(seq)-len(sub)+1):
        testIT = seq[i:i+len(sub)]
        if testIT == sub:
            locations.append(str(i))
    return locations

inputFilePath = sys.argv[1]
sub, theString = parseInput(inputFilePath)
locations = patternMatch(sub, theString)

for i in locations:
    sys.stdout.write(i+" ")

