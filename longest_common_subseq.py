#!/usr/bin/python

''' As you start in the top left, 
        get the number from above
        get the nimber from the left
        if there is a match
            get the number from the diagonal, top left

        find the max of these and put it in that cell
        keep track in another matrix that keeps track of where we came from '''

import sys
import re


class Node:

    def __init__(self,top,left):
        self.left = left
        self.top  = top
        self.direction = 'a'
        self.value = 0

    def set_left(self,left):
        self.left = left

    def set_top(self,top):
        self.top = top


def get_input(path):
    with open (path) as fh:
        top = fh.next().strip()
        left = fh.next().strip()
    print top
    print left
    return top, left

def run_it(top,left):

    graph = [[None]*len(top) for i in range(len(left))]
    for g in graph:
        print g
    for i in range(len(graph)):
        for j in range(len(graph[i])):
            graph[i][j] = Node(top[j],left[i])
    
    k = 0
    for i in range(len(graph)):
        for j in range(len(graph[i])):
            graph[i][j].value = k
            k += 1


    for i in range(len(graph)):
        for j in range(len(graph[i])):
            sys.stdout.write(str(graph[i][j].value) + ' ' )
        print


    backtrack = [[None]*len(top) for i in range(len(left))]
    for i in range(len(graph)):
        for j in range(len(graph[i])):
            backtrack[i][j] = Node(top[j],left[i])
    for i in range(len(graph)):
        if i == 1:
            continue
        for j in range(len(graph[i])):
            if j == 1:
                continue
            max_list = [graph[i-1][j].value,graph[i][j-1].value]
            if graph[i][j].value == graph[i-1][j-1].value:
                max_list.append(graph[i-1][j-1].value + 1)
            graph[i][j].value = max(max_list)

            if graph[i][j].value == graph[i-1][j].value:
                backtrack[i][j].direction = 'down'
            elif graph[i][j].value == graph[i][j-1].value:
                backtrack[i][j].direction = 'right'
            elif graph[i][j].value == (graph[i-1][j-1].value + 1):
                backtrack[i][j].direction = 'diag'

    return backtrack

def output_lcs(backtrack,v,i,j):
    print backtrack[i][j].direction
    print 'in here'
    if i == 0 or j == 0:
        print 'in equals'
        return
    if backtrack[i][j].direction == 'down':
        output_lcs(backtrack,v,i-1,j)
    elif backtrack[i][j].direction == 'right':
        output_lcs(backtrack,v,i,j-1)
    else:
        output_lcs(backtrack,v,i-1,j-1)
        print v[i]
            
top,left = get_input(sys.argv[1])
backtrack = run_it(top,left)
for i in backtrack:
    for j in i:
        sys.stdout.write( j.direction )
    print
output_lcs(backtrack,top,len(left)-1,len(top)-1)
