#!/usr/bin/python

import re
import sys

def lcs(v,w):
    graph = [[0] * (len(w)) for i in range(len(v))]

    max_numb = 0
    max_row = 0
    max_col = 0
    start = ''
    end = ''
    pos = -1

    for i in range(1,len(v)):
        for j in range(1,len(w)):
            if v[i] == w[j]:
                topleft = graph[i-1][j-1] + 1
            else:
                topleft = graph[i-1][j-1] - 2
            graph[i][j] = max(graph[i-1][j]-2,graph[i][j-1]-2,topleft)

            if j == len(w) - 1 and graph[i][j] > max_numb:
                max_numb = graph[i][j]
                max_row = i
                max_col = j
                start = w
                end = v
                pos = i
            elif i == len(v) - 1 and graph[i][j] > max_numb:
                max_numb = graph[i][j]
                max_row = i
                max_col = j
                start = v
                end = w
                pos = j
    temp = graph[max_row][max_col]
    del graph
    return (temp,start[1:],pos)

def run_it(v,w):
    v = '-' + v
    w = '-' + w
    return lcs(v,w)
