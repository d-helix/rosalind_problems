#!/usr/bin/python

import re
import sys


#===========<[ Functions ]>============#

def get_input( path ) :
    with open (path) as fh:
        seq_list = []
        junk = fh.readline()
        for line in fh:
            line = line.strip()
            line = line.split(' | ')
            line = ''.join(line)
            seq_list.append(line)
#seq_list.append(line.strip())
    return seq_list

def make_graph(seq_list):
    graph = dict()
    for seq in seq_list:
        kmer = seq[:-1]
        for seq2 in seq_list:
            kmer2 = seq[1:]
            if kmer[1:] == kmer2[:-1]:
                if kmer not in graph:
                    graph[kmer] = set()
                graph[kmer].add(kmer2)
    return graph
        
def print_graph(graph):
    for g in sorted(graph):
        sys.stdout.write(g + ' -> ')
        sys.stdout.write(','.join(graph[g]))
        print

def run_it(dB_graph):
    nodes = create_nodes(dB_graph) 
#   print_nodes(nodes)
    diffs = find_diffs(nodes)
    path = find_path(nodes,diffs)
    return path

def create_nodes(dB_graph):
    list_nodes = set()
    nodes = []
    for i in dB_graph:
        list_nodes.add(i)
        for j in dB_graph[i]:
            list_nodes.add(j) 
    for node in list_nodes:
        nodes.append(Node(node,dB_graph))
    return nodes

def find_diffs(nodes):
    less_out = []
    for node in nodes:
        if (len(node.incoming) - len(node.outgoing)) != 0:
            less_out.append(node)
    return less_out

def find_path(nodes,diffs):
    if len(diffs) not in [0,2]:
        print 'no path'
        sys.exit()
    start = nodes[0]
    if len(diffs) == 2:
        for node in nodes:
            if (len(node.outgoing) - len(node.incoming)) == 1:
                start = node
    stack = []
    path = []
   #print 'start: ' + start.value
    stack.append(start.value)
    current =  get_node(start.outgoing.pop(0),nodes)
    while len(stack) != 0 or len(current.outgoing) != 0:
        '''print 'path: ' + '->'.join(path)
        print 'stack: ' + '->'.join(stack)
        print 'current: ' + current.value '''
        choices = current.outgoing
        if len(choices) == 0:
            path.append(current.value)
            current = get_node(stack.pop(-1),nodes)
        else:
            stack.append(current.value)
            current = get_node(current.outgoing.pop(0),nodes)
    path.append(current.value)
#   print path[::-1]
    return path[::-1]

def get_node(current,nodes):
    for node in nodes:
        if node.value == current:
            return node
    
def print_nodes(nodes):
    for node in nodes:
        print 'node: ' + node.value
        sys.stdout.write('incoming: ')
        sys.stdout.write(','.join(node.incoming) + '\n')
        sys.stdout.write('outgoing: ')
        sys.stdout.write(','.join(node.outgoing) + '\n')

def create_string(path):
    final = path.pop(0)
    for i in path:
        final += i[-1:] 
    return final 


#==============<[ Classes ]>==============#

class Node:
    def __init__(self,node,dB_graph):
        self.value = node
        self.incoming = []
        self.outgoing = []
        self.find_in_out(dB_graph) 

    def find_in_out(self,dB_graph):
        for i in dB_graph:
            if i == self.value:
                for r in dB_graph[i]:
                    self.outgoing.append(r) 
            for j in dB_graph[i]:
                if j == self.value:
                    self.incoming.append(i)


#===========<[ Main }>============#
 
if __name__ == '__main__':
    seq_list = get_input(sys.argv[1])
    graph = make_graph(seq_list)
    path = run_it(graph) 
    print create_string(path)
