#!/usr/bin/python 

import copy
import re
import sys

#------------- Functions -------------#
def get_input( path ) :
    with open (path) as fh:
        graph = dict()
        for line in fh:
            line = line.strip().split()
            graph[line[0]] = line[2].split(',')
    return graph

def has_one(graph):
    for i in graph:
        if len(graph[i]) != 0:
            return i
    return '-1'

def get_path(final_cycle,i,graph):
    while len(graph[i]) != 0:
        i = graph[i].pop(0)
        inlist = [i]
        final_cycle.append(inlist)

def get_num(graph):
    num = 0
    for i in graph:
        num += len(graph[i])
    return num 
        
def find_cycle(G,current):
    cycle = []
    cycle.append(current)
    while len(G[current]) != 0:
        current = G[current].pop(0) 
        cycle.append(current)
        print cycle
    return cycle    

#-------------- Main ----------------#
graph = get_input(sys.argv[1])




static_graph = copy.deepcopy(graph)

for i in static_graph:
    graph = copy.deepcopy(static_graph)
    cycle = find_cycle(graph,i)
    print get_num(graph)
    if get_num(graph) == 0:
        break
    
print "->".join(cycle)
