#!/usr/bin/python

import re
import sys

def get_input(path):
    with open (path) as fh:
        v = fh.next().strip()
        v = '-' + v
        w = fh.next().strip()
        w = '-' + w
    return v, w

def lcs(v,w):
    graph = [[None] * (len(w)) for i in range(len(v))]
    backtrack = [[None] * (len(w)) for i in range(len(v))]
    
    graph[0][0] = 0
    for i in range(1,len(v)):
        graph[i][0] = 0
    for j in range(1,len(w)):
        graph[0][j] = 0
#    for g in graph:
#        print g

    max_numb = 0
    max_row = 0
    max_col = 0
    start = ''
    end = ''

    for i in range(1,len(v)):
        for j in range(1,len(w)):
            if v[i] == w[j]:
                graph[i][j] = graph[i-1][j-1] + 1
                backtrack[i][j] = 'diag'
            else:
                max_list = []
                max_list.append(graph[i-1][j] - 2)
                max_list.append(graph[i][j-1] - 2)
                max_list.append(graph[i-1][j-1] - 2)
                graph[i][j] = max(max_list)
                if graph[i][j] == graph[i][j-1] - 2:
                    backtrack[i][j] = 'right'
                elif graph[i][j] == graph[i-1][j] - 2: 
                    backtrack[i][j] = 'down'
                elif graph[i][j] == graph[i-1][j-1] - 2:
                    backtrack[i][j] = 'diag'

            if j == len(w) - 1 and graph[i][j] > max_numb:
                max_numb = graph[i][j]
                max_row = i
                max_col = j
                start = w
                end = v
            elif i == len(v) - 1 and graph[i][j] > max_numb:
                max_numb = graph[i][j]
                max_row = i
                max_col = j
                start = v
                end = w

#    for g in graph:
#        print g
#    print
#    for b in backtrack:
#        print b

    return backtrack,graph,max_row,max_col,start,end

v,w = get_input(sys.argv[1])
backtrack,num_graph,row,col,start,end = lcs(v,w)
gr = [['-'] * (len(w) + 1) for i in range(len(v)+1)]
for i in range(1,len(gr)):
    gr[i][0] = v[i-1]
for j in range(1,len(w)+1):
    gr[0][j] = w[j-1]


i = row
j = col
#print
#print i,j
final_one = ''
final_two = ''

go = True
count = 0
while go:
    count += 1
    if i == 0 and j == 0:
        go = False
        gr[i+1][j+1] = 1

    elif backtrack[i][j] == 'down':
        final_one += v[i]
        final_two += '-'
        gr[i+1][j+1] = 1
        i = i-1
    elif backtrack[i][j] == 'right':
        final_one += '-'
        final_two += w[j]
        gr[i+1][j+1] = 1
        j = j - 1    
    elif backtrack[i][j] == 'diag':
        final_one += (v[i])
        final_two += (w[j])
        gr[i+1][j+1] = 1
        i = i - 1
        j = j - 1 
    else:
        gr[i+1][j+1] = 0
        go = False

print
for i in gr:
    print ' '.join(str(x) for x in i)
print
print num_graph[row][col]
print final_two[::-1]
print final_one[::-1]
print
print "Prefix: " + start[1:]
print "Suffix: " + end[1:]
