#!/usr/bin/python

import sys
import re

   
def get_input(path):
    with open (path) as fh:
        v = fh.next().strip()
        v = '-' + v
        w = fh.next().strip()
        w = '-' + w
    return v, w

def lcs(v,w):
    graph = [[None] * (len(w)) for i in range(len(v))]
    backtrack = [[None] * (len(w)) for i in range(len(v))]

    for i in range(0,len(v)):
        graph[i][0] = 0
    for j in range(0,len(w)):
        graph[0][j] = 0
    for i in range(1,len(v)):
        for j in range(1,len(w)):
            max_list = [graph[i-1][j],graph[i][j-1]]
            if v[i] == w[j]:
                max_list.append(graph[i-1][j-1] + 1)
            graph[i][j] = max(max_list)

            if graph[i][j] == graph[i-1][j]:
                backtrack[i][j] = 'down'
            elif graph[i][j] == graph[i][j-1]:
                backtrack[i][j] = 'right'

            elif v[i] == w[j] and graph[i][j] == (graph[i-1][j-1] + 1):
                    backtrack[i][j] = 'diag'

#   for g in graph:
#       print g
#   for b in backtrack:
#       print b

    return backtrack

def output_lcs(backtrack,v,i,j,graph,count):

    count += 1
    if count == 990:
        return
    if i == 0 or j == 0:
        graph[i+1][j+1] = 1
        return
    if backtrack[i][j] == 'down':
        graph[i+1][j+1] = 1
        output_lcs(backtrack,v,i-1,j,graph,count)
    elif backtrack[i][j] == 'right':
        graph[i+1][j+1] = 1
        output_lcs(backtrack,v,i,j-1,graph,count)
    else:
        graph[i+1][j+1] = 1
        output_lcs(backtrack,v,i-1,j-1,graph,count)
        sys.stdout.write(v[i])
    return 0  

v,w = get_input(sys.argv[1])
backtrack = lcs(v,w)
gr = [['-'] * (len(w) + 1) for i in range(len(v)+1)]
for i in range(1,len(gr)):
    gr[i][0] = v[i-1]
for j in range(1,len(w)+1):
    gr[0][j] = w[j-1]

#output_lcs(backtrack,v,len(v)-1,len(w)-1,gr,count)

i = len(v)-1
j = len(w)-1
final = ''
go = True
while go:
    if i == 0 or j == 0:
        go = False
        gr[i+1][j+1] = 1
    elif backtrack[i][j] == 'down':
        gr[i+1][j+1] = 1
        i = i-1
    elif backtrack[i][j] == 'right':
        gr[i+1][j+1] = 1
        j = j - 1    
    else:
        final += (v[i])
        gr[i+1][j+1] = 1
        i = i - 1
        j = j - 1

print final[::-1]
#print
#for i in gr:
#    print ''.join(str(x) for x in i)
