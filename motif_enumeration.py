#!/usr/bin/python

import sys
import re

def GetInput(path):
    with open (path) as f:
        numbers = f.next().split()
        sequence = []
        for line in f:
            sequence.append(line.strip())
    kmer = numbers[0]
    mism = numbers[1]
    return sequence,kmer,mism

def just_do_it():
    all_kmers = []
    nucs = ['A','T','G','C']
    for seq in sequences:
        for i in range(len(seq) - k + 1):
            kmer = seq[i:i+k]
            current_kmers = set()
            for j in range(0,len(kmer)):
                old_nuc = kmer[j]
                for nuc in range(0,4):
                    if nucs[nuc] == old_nuc:
                        continue
                    kmer[j] = nuc[nucs]
                    current_kmers.add("".join(kmer))
                kmer[j] = old_nuc
            all_kmers.add(current_kmers)

inputpath = sys.argv[1]
sequences,kmer_size,mism = GetInput(inputpath)
