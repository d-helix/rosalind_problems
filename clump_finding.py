#! usr/bin/python

import sys
import re

def parseInput(path):
    with open (path) as fh:
        seq = fh.next().strip()
        variables = fh.next().strip()
    k,L,t = variables.split(' ')
    return k,L,t,seq

def doIT (k,L,t,seq):
    kmers = set() 
    for i in range(len(seq) - int(L) + 1):
        counts = dict() 
        clumpLength = seq[i:i + int(L)]
        for j in range(len(clumpLength) - int(k)  + 1):
            kmer = clumpLength[j:j + int(k)]
            if kmer in counts:
                counts[kmer] += 1
            else:
                counts[kmer] = 1
            if counts[kmer] >= int(t):
                kmers.add(kmer)
    return kmers           


inputfile = sys.argv[1]
k,L,t,seq = parseInput(inputfile)
kmers = doIT(k,L,t,seq)

for i in kmers:
    sys.stdout.write(i + " ")

