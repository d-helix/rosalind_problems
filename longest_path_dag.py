#!/usr/bin/python

import re
import sys


class Node:

    def __init__(self,val):
        self.out = []
        self.weight = []
        self.value = val

    def add(self,outnode,weight):
        self.out.append(outnode)
        self.weight.append(weight)

    def find_max(self):
        max_node = self.out[0]
        weight = self.weight[0]
        for i in range(1,len(self.out)):
            if self.out[i] + self.weight[i] > max_node:
                max_node = self.out[i]
                weight = self.weight[i]
        return max_node,weight

def get_input( path ) :
    with open (path) as fh:
        start = int(fh.next().strip())
        end = int(fh.next().strip())
        node_dict = {}
        for line in fh:
            reSearch = re.search(r'(\d+)->(\d+):(\d+)',line)
            outnode = int(reSearch.group(1))
            node = int(reSearch.group(2))
            weight = int(reSearch.group(3))
            if node not in node_dict:
                node_dict[node] = Node(node)
            node_dict[node].add(outnode,weight)    
            if outnode not in node_dict:
                node_dict[outnode] = Node(outnode)
    for node in node_dict:
        print node,node_dict[node].out
    return node_dict,start,end 

def run_it(current,final,start,nodes,total_weight):
    print current.value
    final.append(current.value)
    if current.value == start or len(current.out) == 0:
        return
    cmax,weight = current.find_max()
    total_weight.append(weight)
    run_it(nodes[cmax],final,start,nodes,total_weight)
        
nodes,start,end = get_input(sys.argv[1])
final = []
total_weight = []
run_it(nodes[end],final,start,nodes,total_weight)
final = reversed(final)
print sum(total_weight)
print '->'.join(str(x) for x in final)
