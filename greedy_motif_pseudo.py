#!/usr/bin/python

import sys
import re

def get_input(path):
    return_dict = {}
    with open (path) as f:
        for line in f:
            line = line.strip().split(' ')
            outgoing = []
            for i in line[2]:
                if i != ',':
                    outgoing.append(i) 
            return_dict[line[0]] = outgoing 
    return return_dict


the_map = get_input(sys.argv[1])

print the_map
