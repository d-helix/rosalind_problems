#!/usr/bin/python

import re
import sys


class Node:
    def __init__(self,val,in_id):
        self.node_list = []
        self.value = val
        self.node_id = in_id

    def node_vals(self):
        node_vals = []
        for n in self.node_list:
            node_vals.append(n.value)
        return node_vals

    def get_node(self,gnode):
        for n in self.node_list:
            if n.value == gnode:
                return n


def get_input( path ) :
    with open (path) as fh:
        seq = fh.next().strip()
    return seq

def print_trie(current):
    for n in current.node_list:
        print str(current.node_id) + ' ' +str(n.node_id) + ' ' + n.value
        print_trie(n)

def build_trie(seqs):
    root = Node('root',1)
    current_node = root
    ident = 2
    for seq in seqs:
        for nuc in seq:
            if nuc in current_node.node_vals():
                current_node = current_node.get_node(nuc)
            else:
                current_node.node_list.append(Node(nuc,ident))
                current_node = current_node.get_node(nuc)
                ident += 1
        current_node = root
    return root

def break_seq(seq):
    seqs = []
    for i in range(len(seq) - 1):
        seqs.append(seq[i:])
    return seqs

def condense(current,condensed_nodes,start,building):
    if len(current.node_list) == 1:
        if building == False:
            building = True
            start = current
        condensed_nodes += current.value
        condense(current.node_list[0],condensed_nodes,start,building) 

    elif len(current.node_list) > 1:
        if building == True:
            building = False
            condensed_nodes += current.value
            del start.node_list[:]
            start.node_list = current.node_list
            start.value = condensed_nodes
            condensed_nodes = ''
        for n in current.node_list:
            condense(n,condensed_nodes,start,building)
       
    elif current.value == '$':
        if building == True:
            condensed_nodes += current.value   
            start.value = condensed_nodes
            condensed_nodes = ''
            del start.node_list[:]
            
def print_edges(current):
    for n in current.node_list:
#print n.node_vals()
        print n.value
        print_edges(n)

seq = get_input(sys.argv[1])
seqs = break_seq(seq)
trie = build_trie(seqs)
#print_trie(trie)
for node in trie.node_list:
    cd = '' 
    start = node
    building = False
    condense(node,cd,start,building)
print_edges(trie)
print '$'
