#!/usr/bin/python

import re
import sys


#===========<[ Functions ]>============#

def get_input( path ) :
    with open (path) as fh:
        seq_list = []
        for line in fh:
            seq_list.append(line.strip())
    return seq_list

def get_graph(seq_list):
    graph = dict()
    for seq in seq_list:
        kmer = seq[:-1]
        kmer2 = seq[1:]
        if kmer not in graph:
            graph[kmer] = []
        graph[kmer].append(kmer2)
    return graph

def create_nodes(dB_graph):
    list_nodes = set()
    nodes = []
    for i in dB_graph:
        list_nodes.add(i)
        for j in dB_graph[i]:
            list_nodes.add(j) 
    for node in list_nodes:
        nodes.append(Node(node,dB_graph))
    return nodes

def get_node(current,nodes):
    for node in nodes:
        if node.value == current:
            return node

def find_contigs(nodes):
    starts = start_nodes(nodes)
    contigs = []
    for start in starts:
        for branch in start.outgoing:
            current = get_node(branch,nodes)
            curr_contig = []
            curr_contig.append(start.value)
            contigs.append(contig(current,curr_contig,nodes))
    return contigs   

def start_nodes(nodes):
    starts = []
    for node in nodes:
        if len(node.outgoing) == 1 and len(node.incoming) == 1:
            continue
        elif len(node.outgoing) == 0:
            continue
        else:
            starts.append(node)
    return starts

def contig(current,curr_contig,nodes):
    if len(current.outgoing) > 1:
        curr_contig.append(current.value)
        return curr_contig
    while len(current.outgoing) <= 1:
        if len(current.outgoing) == 0:
            curr_contig.append(current.value)
            break 
        elif len(current.incoming) > 1:
            break
        elif len(current.outgoing) == 1:
            curr_contig.append(current.value)
            current = get_node(current.outgoing[0],nodes) 
    if len(current.outgoing) > 0:
        curr_contig.append(current.value)
    return curr_contig

def get_node(current,nodes):
    for node in nodes:
        if node.value == current:
            return node
             
#------------- Printers --------------#

def print_contigs(contigs):
    final_contigs = []
    for contig in contigs:
        curr_c = ''
        curr_c += contig.pop(0)
        for c in contig:
            curr_c += c[-1:]
        final_contigs.append(curr_c)
    return final_contigs

def print_nodes(nodes):
    for node in nodes:
        print 'node: ' + node.value
        sys.stdout.write('  in: ')
        sys.stdout.write(','.join(node.incoming) + '\n')
        sys.stdout.write('  out: ')
        sys.stdout.write(','.join(node.outgoing) + '\n')

def print_graph(graph):
    for g in sorted(graph):
        sys.stdout.write(g + ' -> ')
        sys.stdout.write(','.join(graph[g]))
        print


#==============<[ Classes ]>==============#

class Node:
    def __init__(self,node,dB_graph):
        self.value = node
        self.incoming = []
        self.outgoing = []
        self.find_in_out(dB_graph) 

    def find_in_out(self,dB_graph):
        for i in dB_graph:
            if i == self.value:
                for r in dB_graph[i]:
                    self.outgoing.append(r) 
            for j in dB_graph[i]:
                if j == self.value:
                    self.incoming.append(i)

        
#=============<[ Main ]>================#
        
if __name__ == '__main__':
    seq_list = get_input(sys.argv[1])
    graph = get_graph(seq_list)
    nodes = create_nodes(graph)
    contigs = find_contigs(nodes)
    print ' '.join(print_contigs(contigs))
