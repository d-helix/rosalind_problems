#!/usr/bin/python

import sys
import re
import string

def GetInput(path):
    with open (path) as f:
        sequence = f.next().strip()
    return sequence

def FindPeptideSeqs(sequence,codons):
    sequence = re.sub(r'U','T',sequence)
    TheCodons = re.findall('.{1,3}',sequence)
    Peptide = ""
    for codon in TheCodons:
        Peptide += codons[codon]
    return Peptide 


codons = {"ATT":"I","ATC":"I","ATA":"I",
          "CTT":"L","CTC":"L","CTA":"L","CTG":"L","TTA":"L","TTG":"L",
          "GTT":"V","GTC":"V","GTA":"V","GTG":"V",
          "TTT":"F","TTC":"F",
          "ATG":"M",
          "TGT":"C","TGC":"C",
          "GCT":"A","GCC":"A","GCA":"A","GCG":"A",
          "GGT":"G","GGC":"G","GGA":"G","GGG":"G",
          "CCT":"P","CCC":"P","CCA":"P","CCG":"P",
          "ACT":"T","ACC":"T","ACA":"T","ACG":"T",
          "TCT":"S","TCC":"S","TCA":"S","TCG":"S","AGT":"S","AGC":"S",
          "TAT":"Y","TAC":"Y",
          "TGG":"W",
          "CAA":"Q","CAG":"Q",
          "AAT":"N","AAC":"N",
          "CAT":"H","CAC":"H",
          "GAA":"E","GAG":"E",
          "GAT":"D","GAC":"D",
          "AAA":"K","AAG":"K",
          "CGT":"R","CGC":"R","CGA":"R","CGG":"R","AGA":"R","AGG":"R",
          "TAA":"","TAG":"","TGA":""}

sequence = GetInput(sys.argv[1])          
Peptide = FindPeptideSeqs(sequence, codons)
print Peptide
