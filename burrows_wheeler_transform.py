#!/usr/bin/python

import re
import sys

def get_input(path):
    with open (path) as fh:
        seq = fh.next().strip()
    return seq

def run_it(seq):
    seqs = []
    ends = []
    for i in range(len(seq)):
        ins = seq[i:] + seq[:i]
        seqs.append(ins)
    seqs = sorted(seqs)
    for i in seqs:
        ends.append(i[-1])
    return ends
    

seq = get_input(sys.argv[1])
ends = run_it(seq)
print ''.join(ends)
