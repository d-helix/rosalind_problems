#!/usr/bin/python 

'''
Algorithm for directed graphs:
1) Start with an empty stack and an empty circuit (eulerian path).
- If all vertices have same out-degrees as in-degrees - choose any of them.
- If all but 2 vertices have same out-degree as in-degree,
    and one of those 2 vertices has out-degree with one greater than its in-degree,
    and the other has in-degree with one greater than its out-degree -
    then choose the vertex that has its out-degree with one greater than its in-degree.
- Otherwise no euler circuit or path exists.
2) If current vertex has no out-going edges (i.e. neighbors) -
    add it to circuit, remove the last vertex from the stack and set it as the current one.
    Otherwise (in case it has out-going edges, i.e. neighbors) -
    add the vertex to the stack, take any of its neighbors,
    remove the edge between that vertex and selected neighbor,
    and set that neighbor as the current vertex.
3) Repeat step 2 until the current vertex has no more out-going edges (neighbors) and the stack is empty.
'''

import re
import sys


#=============<[ Functions ]>=============#

def get_input( path ) :
    with open (path) as fh:
        graph = dict()
        for line in fh:
            line = line.strip().split()
            graph[line[0]] = line[2].split(',')
    return graph

def run_it(dB_graph):
    nodes = create_nodes(dB_graph) 
#   print_nodes(nodes)
    diffs = find_diffs(nodes)
    path = find_path(nodes,diffs)
    return path

def create_nodes(dB_graph):
    list_nodes = set()
    nodes = []
    for i in dB_graph:
        list_nodes.add(i)
        for j in dB_graph[i]:
            list_nodes.add(j) 
    for node in list_nodes:
        nodes.append(Node(node,dB_graph))
    return nodes

def find_diffs(nodes):
    less_out = []
    for node in nodes:
        if (len(node.incoming) - len(node.outgoing)) != 0:
            less_out.append(node)
    return less_out

def find_path(nodes,diffs):
    if len(diffs) not in [0,2]:
        print 'no path'
        sys.exit()
    start = nodes[0]
    if len(diffs) == 2:
        for node in nodes:
            if (len(node.outgoing) - len(node.incoming)) == 1:
                start = node
    stack = []
    path = []
    print 'start: ' + start.value
    stack.append(start.value)
    current =  get_node(start.outgoing.pop(0),nodes)
    while len(stack) != 0 or len(current.outgoing) != 0:
        print 'path: ' + '->'.join(path)
        print 'stack: ' + '->'.join(stack)
        print 'current: ' + current.value
        choices = current.outgoing
        if len(choices) == 0:
            path.append(current.value)
            current = get_node(stack.pop(-1),nodes)
        else:
            stack.append(current.value)
            current = get_node(current.outgoing.pop(0),nodes)
    path.append(current.value)
#   print path[::-1]
    return path[::-1]

def get_node(current,nodes):
    for node in nodes:
        if node.value == current:
            return node
    
def print_nodes(nodes):
    for node in nodes:
        print 'node: ' + node.value
        sys.stdout.write('incoming: ')
        sys.stdout.write(','.join(node.incoming) + '\n')
        sys.stdout.write('outgoing: ')
        sys.stdout.write(','.join(node.outgoing) + '\n')


#==============<[ Classes ]>==============#

class Node:
    def __init__(self,node,dB_graph):
        self.value = node
        self.incoming = []
        self.outgoing = []
        self.find_in_out(dB_graph) 

    def find_in_out(self,dB_graph):
        for i in dB_graph:
            if i == self.value:
                for r in dB_graph[i]:
                    self.outgoing.append(r) 
            for j in dB_graph[i]:
                if j == self.value:
                    self.incoming.append(i)
        
    
#===============<[ Main ]>================#

if __name__ == '__main__':
    dB_graph = get_input(sys.argv[1])
    print '->'.join(run_it(dB_graph))





