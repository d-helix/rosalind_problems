#!/usr/bin/python

import sys
import re

def get_input( path ) :
    with open (path) as fh:
        k = int(fh.next().strip())
        seq = fh.next().strip()
    return seq, k

def create_kmers(seq,k):
    kmer_list = []
    for i in xrange(len(seq) - k + 1):
        kmer = seq[i:i + k] 
        kmer_list.append(kmer)
    return sorted(kmer_list)

seq,k = get_input(sys.argv[1])
for i in create_kmers(seq,k):
    print i
 
