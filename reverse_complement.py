#! usr/bin/python 

import sys
import re

def getSeq (inputfile):
    with open (inputfile) as fh:
        seq = fh.next().strip()
    return seq

def reverseIt (seq):
    revseq = seq[::-1]
    revseq = re.sub('A','t',revseq)
    revseq = re.sub('T','A',revseq)
    revseq = re.sub('G','c',revseq)
    revseq = re.sub('C','g',revseq)
    revseq = revseq.upper()
    return revseq


inputfile = sys.argv[1]

seq = getSeq(inputfile)
reverseSeq = reverseIt(seq)

print  reverseSeq
