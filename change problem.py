#!/usr/bin/python

import re
import sys


def get_input( path ) :
    with open (path) as fh:
        money = int(fh.next().strip())
        coins = fh.next().strip().split(',')
        coins = map(int,coins)
    return money,coins

def DPChange(money,coins):
    min_coins = []
    min_coins.append(0)
    for m in range(1,money + 1):
        min_val = float('inf')
#       print 'm: ' + str(m)
        for coin in coins:
#           print 'coin: ' + str(coin)
#           print 'm: ' + str(m)
            if coin <= m:
#               print 'in first if'
#               print min_coins[m - coin] + 1
                if (min_coins[m - coin] + 1) < min_val:
                    min_val = (min_coins[m-coin] + 1)
        min_coins.append(min_val) 
#       print min_coins
    return min_coins

def min_num_coins(min_coins,m,coins):
    min_list = []
    for coin in coins:
        min_list[m - coin] + 1 
            

if __name__ == "__main__":
    money,coins = get_input(sys.argv[1])
    print DPChange(money,coins)[-1]

    
